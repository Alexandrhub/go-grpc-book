package service

import (
	"context"

	"gitlab.com/Alexandrhub/go-grpc-book/pkg/pb"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type AuthServer struct {
	userStore  UserStore
	jwtManager *JWTManager
	pb.UnimplementedAuthServiceServer
}

func NewAuthServer(userStore UserStore, jwtManager *JWTManager) *AuthServer {
	return &AuthServer{
		userStore:  userStore,
		jwtManager: jwtManager,
	}
}

func (s *AuthServer) Login(ctx context.Context, req *pb.LoginRequest) (*pb.LoginResponse, error) {
	user, err := s.userStore.Find(req.GetUsername())
	if err != nil {
		return nil, status.Errorf(
			codes.Internal,
			"failed to find user: %v",
			err,
		)
	}

	if user == nil || !user.IsCorrectPassword(req.GetPassword()) {
		return nil, status.Errorf(
			codes.NotFound,
			"invalid username/password",
		)
	}

	token, err := s.jwtManager.GenerateToken(user)
	if err != nil {
		return nil, status.Errorf(
			codes.Internal,
			"failed to generate access token",
		)
	}

	return &pb.LoginResponse{
		AccessToken: token,
	}, nil
}
