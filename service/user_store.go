package service

import (
	"fmt"
	"sync"
)

type UserStore interface {
	Save(user *User) error
	Find(username string) (*User, error)
}

type InMemoryUserStore struct {
	mu    sync.RWMutex
	users map[string]*User
}

func NewInMemoryUserStore() *InMemoryUserStore {
	return &InMemoryUserStore{
		users: make(map[string]*User),
	}
}

func (store *InMemoryUserStore) Save(user *User) error {
	store.mu.Lock()
	defer store.mu.Unlock()

	if store.users[user.Username] != nil {
		return fmt.Errorf("user already exists: %q", user.Username)
	}

	store.users[user.Username] = user.Clone()
	return nil
}

func (store *InMemoryUserStore) Find(username string) (*User, error) {
	store.mu.RLock()
	defer store.mu.RUnlock()

	user := store.users[username]
	if user == nil {
		return nil, fmt.Errorf("user not found: %q", username)
	}

	return user.Clone(), nil
}
