# go-grpc-book

## Getting started

remove -nodes from gen.sh if want to encrypt .pem for tls

and add secrets in .env / aws secrets manager/ hashicorp vault(vaultproject.io)

to switch TLS on/off - add/remove flag -tls to Makefile

make cert - for generate tls data

copy server-cert.pem, server-key.pem, ca-cert.pem if want nginx load balance(and add ssl, ssl_sertif location to nginx.conf)

choose nginx.conf and set it in /etc/nginx/nginx.conf(linux)

call evans -r repl -p "your grpc host(8080 for example)" --header Authorization="Your auth token"
