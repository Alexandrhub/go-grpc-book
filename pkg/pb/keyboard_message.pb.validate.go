// Code generated by protoc-gen-validate. DO NOT EDIT.
// source: proto/keyboard_message.proto

package pb

import (
	"bytes"
	"errors"
	"fmt"
	"net"
	"net/mail"
	"net/url"
	"regexp"
	"sort"
	"strings"
	"time"
	"unicode/utf8"

	"google.golang.org/protobuf/types/known/anypb"
)

// ensure the imports are used
var (
	_ = bytes.MinRead
	_ = errors.New("")
	_ = fmt.Print
	_ = utf8.UTFMax
	_ = (*regexp.Regexp)(nil)
	_ = (*strings.Reader)(nil)
	_ = net.IPv4len
	_ = time.Duration(0)
	_ = (*url.URL)(nil)
	_ = (*mail.Address)(nil)
	_ = anypb.Any{}
	_ = sort.Sort
)

// Validate checks the field values on Keyboard with the rules defined in the
// proto definition for this message. If any rules are violated, the first
// error encountered is returned, or nil if there are no violations.
func (m *Keyboard) Validate() error {
	return m.validate(false)
}

// ValidateAll checks the field values on Keyboard with the rules defined in
// the proto definition for this message. If any rules are violated, the
// result is a list of violation errors wrapped in KeyboardMultiError, or nil
// if none found.
func (m *Keyboard) ValidateAll() error {
	return m.validate(true)
}

func (m *Keyboard) validate(all bool) error {
	if m == nil {
		return nil
	}

	var errors []error

	// no validation rules for Layout

	// no validation rules for Backlit

	if len(errors) > 0 {
		return KeyboardMultiError(errors)
	}

	return nil
}

// KeyboardMultiError is an error wrapping multiple validation errors returned
// by Keyboard.ValidateAll() if the designated constraints aren't met.
type KeyboardMultiError []error

// Error returns a concatenation of all the error messages it wraps.
func (m KeyboardMultiError) Error() string {
	var msgs []string
	for _, err := range m {
		msgs = append(msgs, err.Error())
	}
	return strings.Join(msgs, "; ")
}

// AllErrors returns a list of validation violation errors.
func (m KeyboardMultiError) AllErrors() []error { return m }

// KeyboardValidationError is the validation error returned by
// Keyboard.Validate if the designated constraints aren't met.
type KeyboardValidationError struct {
	field  string
	reason string
	cause  error
	key    bool
}

// Field function returns field value.
func (e KeyboardValidationError) Field() string { return e.field }

// Reason function returns reason value.
func (e KeyboardValidationError) Reason() string { return e.reason }

// Cause function returns cause value.
func (e KeyboardValidationError) Cause() error { return e.cause }

// Key function returns key value.
func (e KeyboardValidationError) Key() bool { return e.key }

// ErrorName returns error name.
func (e KeyboardValidationError) ErrorName() string { return "KeyboardValidationError" }

// Error satisfies the builtin error interface
func (e KeyboardValidationError) Error() string {
	cause := ""
	if e.cause != nil {
		cause = fmt.Sprintf(" | caused by: %v", e.cause)
	}

	key := ""
	if e.key {
		key = "key for "
	}

	return fmt.Sprintf(
		"invalid %sKeyboard.%s: %s%s",
		key,
		e.field,
		e.reason,
		cause)
}

var _ error = KeyboardValidationError{}

var _ interface {
	Field() string
	Reason() string
	Key() bool
	Cause() error
	ErrorName() string
} = KeyboardValidationError{}
