// Code generated by protoc-gen-validate. DO NOT EDIT.
// source: proto/processor_message.proto

package pb

import (
	"bytes"
	"errors"
	"fmt"
	"net"
	"net/mail"
	"net/url"
	"regexp"
	"sort"
	"strings"
	"time"
	"unicode/utf8"

	"google.golang.org/protobuf/types/known/anypb"
)

// ensure the imports are used
var (
	_ = bytes.MinRead
	_ = errors.New("")
	_ = fmt.Print
	_ = utf8.UTFMax
	_ = (*regexp.Regexp)(nil)
	_ = (*strings.Reader)(nil)
	_ = net.IPv4len
	_ = time.Duration(0)
	_ = (*url.URL)(nil)
	_ = (*mail.Address)(nil)
	_ = anypb.Any{}
	_ = sort.Sort
)

// Validate checks the field values on CPU with the rules defined in the proto
// definition for this message. If any rules are violated, the first error
// encountered is returned, or nil if there are no violations.
func (m *CPU) Validate() error {
	return m.validate(false)
}

// ValidateAll checks the field values on CPU with the rules defined in the
// proto definition for this message. If any rules are violated, the result is
// a list of violation errors wrapped in CPUMultiError, or nil if none found.
func (m *CPU) ValidateAll() error {
	return m.validate(true)
}

func (m *CPU) validate(all bool) error {
	if m == nil {
		return nil
	}

	var errors []error

	// no validation rules for Brand

	// no validation rules for Name

	// no validation rules for NumberCores

	// no validation rules for NumberThreads

	// no validation rules for MinGhz

	// no validation rules for MaxGhz

	if len(errors) > 0 {
		return CPUMultiError(errors)
	}

	return nil
}

// CPUMultiError is an error wrapping multiple validation errors returned by
// CPU.ValidateAll() if the designated constraints aren't met.
type CPUMultiError []error

// Error returns a concatenation of all the error messages it wraps.
func (m CPUMultiError) Error() string {
	var msgs []string
	for _, err := range m {
		msgs = append(msgs, err.Error())
	}
	return strings.Join(msgs, "; ")
}

// AllErrors returns a list of validation violation errors.
func (m CPUMultiError) AllErrors() []error { return m }

// CPUValidationError is the validation error returned by CPU.Validate if the
// designated constraints aren't met.
type CPUValidationError struct {
	field  string
	reason string
	cause  error
	key    bool
}

// Field function returns field value.
func (e CPUValidationError) Field() string { return e.field }

// Reason function returns reason value.
func (e CPUValidationError) Reason() string { return e.reason }

// Cause function returns cause value.
func (e CPUValidationError) Cause() error { return e.cause }

// Key function returns key value.
func (e CPUValidationError) Key() bool { return e.key }

// ErrorName returns error name.
func (e CPUValidationError) ErrorName() string { return "CPUValidationError" }

// Error satisfies the builtin error interface
func (e CPUValidationError) Error() string {
	cause := ""
	if e.cause != nil {
		cause = fmt.Sprintf(" | caused by: %v", e.cause)
	}

	key := ""
	if e.key {
		key = "key for "
	}

	return fmt.Sprintf(
		"invalid %sCPU.%s: %s%s",
		key,
		e.field,
		e.reason,
		cause)
}

var _ error = CPUValidationError{}

var _ interface {
	Field() string
	Reason() string
	Key() bool
	Cause() error
	ErrorName() string
} = CPUValidationError{}

// Validate checks the field values on GPU with the rules defined in the proto
// definition for this message. If any rules are violated, the first error
// encountered is returned, or nil if there are no violations.
func (m *GPU) Validate() error {
	return m.validate(false)
}

// ValidateAll checks the field values on GPU with the rules defined in the
// proto definition for this message. If any rules are violated, the result is
// a list of violation errors wrapped in GPUMultiError, or nil if none found.
func (m *GPU) ValidateAll() error {
	return m.validate(true)
}

func (m *GPU) validate(all bool) error {
	if m == nil {
		return nil
	}

	var errors []error

	// no validation rules for Brand

	// no validation rules for Name

	// no validation rules for MinGhz

	// no validation rules for MaxGhz

	if all {
		switch v := interface{}(m.GetMemory()).(type) {
		case interface{ ValidateAll() error }:
			if err := v.ValidateAll(); err != nil {
				errors = append(errors, GPUValidationError{
					field:  "Memory",
					reason: "embedded message failed validation",
					cause:  err,
				})
			}
		case interface{ Validate() error }:
			if err := v.Validate(); err != nil {
				errors = append(errors, GPUValidationError{
					field:  "Memory",
					reason: "embedded message failed validation",
					cause:  err,
				})
			}
		}
	} else if v, ok := interface{}(m.GetMemory()).(interface{ Validate() error }); ok {
		if err := v.Validate(); err != nil {
			return GPUValidationError{
				field:  "Memory",
				reason: "embedded message failed validation",
				cause:  err,
			}
		}
	}

	if len(errors) > 0 {
		return GPUMultiError(errors)
	}

	return nil
}

// GPUMultiError is an error wrapping multiple validation errors returned by
// GPU.ValidateAll() if the designated constraints aren't met.
type GPUMultiError []error

// Error returns a concatenation of all the error messages it wraps.
func (m GPUMultiError) Error() string {
	var msgs []string
	for _, err := range m {
		msgs = append(msgs, err.Error())
	}
	return strings.Join(msgs, "; ")
}

// AllErrors returns a list of validation violation errors.
func (m GPUMultiError) AllErrors() []error { return m }

// GPUValidationError is the validation error returned by GPU.Validate if the
// designated constraints aren't met.
type GPUValidationError struct {
	field  string
	reason string
	cause  error
	key    bool
}

// Field function returns field value.
func (e GPUValidationError) Field() string { return e.field }

// Reason function returns reason value.
func (e GPUValidationError) Reason() string { return e.reason }

// Cause function returns cause value.
func (e GPUValidationError) Cause() error { return e.cause }

// Key function returns key value.
func (e GPUValidationError) Key() bool { return e.key }

// ErrorName returns error name.
func (e GPUValidationError) ErrorName() string { return "GPUValidationError" }

// Error satisfies the builtin error interface
func (e GPUValidationError) Error() string {
	cause := ""
	if e.cause != nil {
		cause = fmt.Sprintf(" | caused by: %v", e.cause)
	}

	key := ""
	if e.key {
		key = "key for "
	}

	return fmt.Sprintf(
		"invalid %sGPU.%s: %s%s",
		key,
		e.field,
		e.reason,
		cause)
}

var _ error = GPUValidationError{}

var _ interface {
	Field() string
	Reason() string
	Key() bool
	Cause() error
	ErrorName() string
} = GPUValidationError{}
