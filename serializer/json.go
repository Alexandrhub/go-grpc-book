package serializer

import (
	"google.golang.org/protobuf/encoding/protojson"
	"google.golang.org/protobuf/proto"
)

// ProtobufToJSON converts protocol buffer message to JSON string
func ProtobufToJSON(message proto.Message) (string, error) {
	res := protojson.MarshalOptions{
		Indent:          "  ",
		UseProtoNames:   true,
		EmitUnpopulated: true,
	}

	data, err := res.Marshal(message)

	if err != nil {
		return "", err
	}

	return string(data), nil
}

// JSONToProtobufMessage converts JSON string to protocol buffer message
func JSONToProtobufMessage(data string, message proto.Message) error {
	res := protojson.UnmarshalOptions{
		DiscardUnknown: true,
	}

	err := res.Unmarshal([]byte(data), message)

	if err != nil {
		return err
	}

	return nil
}
