gen:
	protoc --go_out=. --go-grpc_out=./pb proto/*.proto

clean:
	rm pb/*.go

server:
	go run cmd/server/main.go -port 8080

rest:
	go run cmd/server/main.go -port 8081 -type rest -endpoint 0.0.0.0:8080

server1:
	go run cmd/server/main.go -port 50051

server2:
	go run cmd/server/main.go -port 50052

server1-tls:
	go run cmd/server/main.go -port 50051 -tls

server2-tls:
	go run cmd/server/main.go -port 50052 -tls

client:
	go run cmd/client/main.go -address 0.0.0.0:8080 

client-tls:
	go run cmd/client/main.go -address 0.0.0.0:8080 -tls

test:
	go test ./... --cover -v -race

cert:
	cd cert; ./gen.sh; cd ..

evans:
	evans -r repl -p 8080

.PHONY: gen clean server client test cert server1 server2 client-tls server1-tls server2-tls evans rest